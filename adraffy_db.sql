--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1 (Debian 12.1-1.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: supplier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.supplier (
    id integer NOT NULL,
    nama character varying NOT NULL
);


ALTER TABLE public.supplier OWNER TO postgres;

--
-- Name: free_supplier_for_obat(integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.free_supplier_for_obat(obat_id integer) RETURNS SETOF public.supplier
    LANGUAGE plpgsql STABLE
    AS $$
begin
	return query 
		select s.id, s.nama from supplier s 
		where s.id not in 
			(select s2."supplierId" from supply s2 where s2."obatId" = obat_id);
end; $$;


ALTER FUNCTION public.free_supplier_for_obat(obat_id integer) OWNER TO postgres;

--
-- Name: obat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.obat (
    id integer NOT NULL,
    nama character varying NOT NULL
);


ALTER TABLE public.obat OWNER TO postgres;

--
-- Name: obat_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.obat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.obat_id_seq OWNER TO postgres;

--
-- Name: obat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.obat_id_seq OWNED BY public.obat.id;


--
-- Name: obats_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.obats_view AS
SELECT
    NULL::integer AS id,
    NULL::character varying AS nama,
    NULL::bigint AS total_supplier;


ALTER TABLE public.obats_view OWNER TO postgres;

--
-- Name: setting; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.setting (
    id integer NOT NULL,
    key character varying NOT NULL,
    data jsonb NOT NULL
);


ALTER TABLE public.setting OWNER TO postgres;

--
-- Name: setting_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.setting_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.setting_id_seq OWNER TO postgres;

--
-- Name: setting_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.setting_id_seq OWNED BY public.setting.id;


--
-- Name: supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.supplier_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.supplier_id_seq OWNER TO postgres;

--
-- Name: supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.supplier_id_seq OWNED BY public.supplier.id;


--
-- Name: suppliers_view; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.suppliers_view AS
SELECT
    NULL::integer AS id,
    NULL::character varying AS nama,
    NULL::bigint AS total_obat;


ALTER TABLE public.suppliers_view OWNER TO postgres;

--
-- Name: supply; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.supply (
    id integer NOT NULL,
    "obatId" integer NOT NULL,
    "supplierId" integer NOT NULL,
    harga integer NOT NULL,
    diskon real NOT NULL,
    expire integer NOT NULL,
    stok character varying NOT NULL,
    jarak real NOT NULL,
    "sistemPembayaran" character varying NOT NULL
);


ALTER TABLE public.supply OWNER TO postgres;

--
-- Name: supply_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.supply_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.supply_id_seq OWNER TO postgres;

--
-- Name: supply_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.supply_id_seq OWNED BY public.supply.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying NOT NULL,
    password character varying NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: obat id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.obat ALTER COLUMN id SET DEFAULT nextval('public.obat_id_seq'::regclass);


--
-- Name: setting id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.setting ALTER COLUMN id SET DEFAULT nextval('public.setting_id_seq'::regclass);


--
-- Name: supplier id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supplier ALTER COLUMN id SET DEFAULT nextval('public.supplier_id_seq'::regclass);


--
-- Name: supply id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supply ALTER COLUMN id SET DEFAULT nextval('public.supply_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Data for Name: obat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.obat (id, nama) FROM stdin;
13	normalize
9	danger day
14	cyclone eyes
16	foasasas
17	amoxicilin
\.


--
-- Data for Name: setting; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.setting (id, key, data) FROM stdin;
\.


--
-- Data for Name: supplier; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.supplier (id, nama) FROM stdin;
1	INDOFARMA GLOBAL MEDIKA
2	KIMIA FARMA
6	ENSEVAL
7	DOS NI ROHA
8	SAPTA SARITAMA
9	GUNUNG MAS JAYA
10	PT PENTHA VALENT
11	PT ANUGERAH PHARMINDO LESTARI
12	PT ADITYA FARMA TAMA
13	PT CEMPAKA INDAH MURNI
14	PT MARGA NUSANTARA JAYA
15	PT BAYUMAS JAYA MANDIRIBENAR
16	PT TEMPO
17	PT ANUGERAH ARGON MEDIKA
18	PT BIMA SAN PRIMA
19	PT SAPTA SARI TAMA
\.


--
-- Data for Name: supply; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.supply (id, "obatId", "supplierId", harga, diskon, expire, stok, jarak, "sistemPembayaran") FROM stdin;
1	14	7	2000	0	1	Full	2	Cepat
3	9	2	2000	0	4	Full	4	Sedang
7	16	6	2000	0	1	Full	2	Cepat
6	9	7	12000	3.5	7	Full	3	Sedang
4	9	1	8000	5	1	Half	1	Cepat
5	9	6	2000	2.02	3	Half	5.5	Sedang
10	17	2	42750	0	3	Full	1.7	Lama
11	17	1	33400	0	3	Full	4.6	Lama
8	17	6	28000	0	3	Full	4	Lama
9	17	6	68600	0	2	Full	4.3	Lama
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, username, password) FROM stdin;
1	adminzero	adminzero
4	realadmin	realadmin
\.


--
-- Name: obat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.obat_id_seq', 18, true);


--
-- Name: setting_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.setting_id_seq', 1, false);


--
-- Name: supplier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.supplier_id_seq', 19, true);


--
-- Name: supply_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.supply_id_seq', 15, true);


--
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 4, true);


--
-- Name: supply PK_11dcdc2def0eb6d10ed3ae0180d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supply
    ADD CONSTRAINT "PK_11dcdc2def0eb6d10ed3ae0180d" PRIMARY KEY (id);


--
-- Name: supplier PK_2bc0d2cab6276144d2ff98a2828; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supplier
    ADD CONSTRAINT "PK_2bc0d2cab6276144d2ff98a2828" PRIMARY KEY (id);


--
-- Name: obat PK_75294be1aa56e994d68c3a1d16d; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.obat
    ADD CONSTRAINT "PK_75294be1aa56e994d68c3a1d16d" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: setting PK_fcb21187dc6094e24a48f677bed; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.setting
    ADD CONSTRAINT "PK_fcb21187dc6094e24a48f677bed" PRIMARY KEY (id);


--
-- Name: setting UQ_1c4c95d773004250c157a744d6e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.setting
    ADD CONSTRAINT "UQ_1c4c95d773004250c157a744d6e" UNIQUE (key);


--
-- Name: user UQ_78a916df40e02a9deb1c4b75edb; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_78a916df40e02a9deb1c4b75edb" UNIQUE (username);


--
-- Name: obats_view _RETURN; Type: RULE; Schema: public; Owner: postgres
--

CREATE OR REPLACE VIEW public.obats_view AS
 SELECT ob.id,
    ob.nama,
    count(supply.id) AS total_supplier
   FROM (public.obat ob
     LEFT JOIN public.supply ON ((supply."obatId" = ob.id)))
  GROUP BY ob.id
  ORDER BY ob.nama;


--
-- Name: suppliers_view _RETURN; Type: RULE; Schema: public; Owner: postgres
--

CREATE OR REPLACE VIEW public.suppliers_view AS
 SELECT sup.id,
    sup.nama,
    count(supply.id) AS total_obat
   FROM (public.supplier sup
     LEFT JOIN public.supply ON ((supply."supplierId" = sup.id)))
  GROUP BY sup.id
  ORDER BY sup.nama;


--
-- Name: supply fk_obat_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supply
    ADD CONSTRAINT fk_obat_id FOREIGN KEY ("obatId") REFERENCES public.obat(id) ON DELETE CASCADE;


--
-- Name: supply fk_supplier_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.supply
    ADD CONSTRAINT fk_supplier_id FOREIGN KEY ("supplierId") REFERENCES public.supplier(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

